# Python-Basic Datatypes and Data structures

## Basic Data-types

Every value in Python has a datatype. Since everything is an object in Python programming, data types are actually classes and variables are instance (object) of these classes.
There are various data types in Python. Some of the important types are listed below.

### Python Numbers

#### 1. Integers

1. In Python 3, there is no limit to how long an __*integer*__ value can be. It is constrained by the amount of memory your system has.

   ```Python3
   >>> print(123123123123123123123123123123123123123123123123 + 1)
   123123123123123123123123123123123123123123123124
   ```

2. Python interprets a sequence of decimal digits without any prefix to be a decimal number but we can represent decimal digits with another base.

   1. Binary (base 2) :- 0b (zero + lowercase letter 'b') OR 0B (zero + uppercase letter 'B')
   2. Octal (base 8) :- 0o (zero + lowercase letter 'o') OR 0O (zero + uppercase letter 'O')
   3. Hexadecimal (base 16) :- 0x (zero + lowercase letter 'x') OR 0X (zero + uppercase letter 'X')

   ```Python3
   >>> print(0o10)
   8

   >>> print(0x10)
   16

   >>> print(0b10)
   2
   ```

#### 2. Floating-Point Numbers

1. __*Float*__ values are specified with a decimal point.

    ```Python3
    >>> 4.2
    4.2

    >>> type(4.2)
    <class 'float'>

    >>> 4
    4.0
    ```

2. Optionally, the character e or E followed by a positive or negative integer may be appended to specify [scientific notation.](https://en.wikipedia.org/wiki/Scientific_notation)

   ```Python3
   >>> .4e7
   4000000.0

   >>> type(.4e7)
   <class 'float'>

   >>> 4.2e-4
   0.00042
   ```

3. A floating-point number is accurate up to 15 decimal places.

#### 3. Complex Numbers

1. Complex numbers are written in the form, __*x + yj*__, where x is the real part and y is the imaginary part.

   ```Python3
   >>> 2+3j
   (2+3j)

   >>> type(2+3j)
   <class 'complex'>
   ```

### Python String

1. Strings are sequences of character data. The string type in Python is called __*str*__.
2. We can use single quotes or double quotes to represent strings.
3. Multi-line strings can be denoted using triple quotes, **'''** or **"""**.
4. A string in Python can contain as many characters as you wish. The only limit is your machine’s memory resources. A string can also be empty.

   ```Python3
   >>> print("I am a string.")
   I am a string.

   >>> type("I am a string.")
   <class 'str'>

   >>> print('I am too.')
   I am too.

   >>> type('I am too.')
   <class 'str'>
   >>> ''
   ''
   ```

5. Strings are immutable meaning, the characters of a string can not be altered.
6. The slicing operator __[]__ can be used with strings.

   ```Python3
   >>> s = 'Hello world!'
   >>> print("s[4] = ", s[4])
   s[4] =  o

   >>> print("s[6:11] = ", s[6:11])
   s[6:11] =  world

   >>> s[5] ='d'
   Traceback (most recent call last):
      File "<string>", line 11, in <module>
   TypeError: 'str' object does not support item assignment
   ```

## Data-structures

A data structure is a collection of different forms and different types of data that has a set of specific operations that can be performed. It is a collection of data types. It is a way of organizing the items in terms of memory, and also the way of accessing each item through some defined logic.

### Python List

1. List is an ordered sequence of items.
2. List is one of the most used datatype in Python and is very flexible.
3. All the items in a list do not need to be of the same type.
4. Lists are mutable, meaning, the value of elements of a list can be altered.
5. Declaring a list is pretty straight forward. Items separated by commas are enclosed within brackets [].

   ```Python3
   >>> a = [1, 2.2, 'python']
   ```

6. We can use the slicing operator [] to extract an item or a range of items from a list. The index starts from 0 in Python.

   ```Python3
   >>> a = [5,10,15,20,25,30,35,40]
   >>> print("a[2] = ", a[2])
   a[2] =  15

   >>> print("a[0:3] = ", a[0:3])
   a[0:3] =  [5, 10, 15]

   >>> print("a[5:] = ", a[5:])
   a[5:] =  [30, 35, 40]
   ```

### Python Tuple

1. Tuple is an ordered sequence of items same as a list.
2. Tuples are immutable.
3. Tuples once created cannot be modified.
4. Tuples are used to write-protect data and are usually faster than lists as they cannot change dynamically.
5. Tuple is defined within parentheses () where items are separated by commas.

   ```Python3
   >>> t = (5,'program', 1+3j)
   ```

6. We can use the slicing operator [] to extract items but we cannot change its value.

   ```Python3
   >>> t = (5,'program', 1+3j)
   >>> print("t[1] = ", t[1])
   t[1] =  program

   >>> print("t[0:3] = ", t[0:3])
   t[0:3] =  (5, 'program', (1+3j))

   >>> t[0] = 10
   Traceback (most recent call last):
    File "test.py", line 11, in <module>
    t[0] = 10
   TypeError: 'tuple' object does not support item assignment
   ```

### Python Set

1. Set is an unordered collection of unique items.
2. Set is defined by values separated by comma inside braces {}.
3. Items in a set are not ordered.

   ```Python3
   >>> a = {5,2,3,1,4}
   >>> print("a = ", a)
   a =  {1, 2, 3, 4, 5}

   >>> print(type(a))
   <class 'set'>
   ```

4. We can perform set operations like union, intersection on two sets.
5. Sets have unique values. They eliminate duplicates.

   ```Python3
   >>> a = {1,2,2,3,3,3}
   >>> print(a)
   {1, 2, 3}
   ```

6. Since, set are unordered collection, indexing has no meaning. Hence, the slicing operator [] does not work.

   ```Python3
   >>> a = {1,2,3}
   >>> a[1]
   Traceback (most recent call last):
     File "<string>", line 301, in runcode
     File "<interactive input>", line 1, in <module>
   TypeError: 'set' object does not support indexing
   ```

### Python Dictionary

1. Dictionary is an unordered collection of key-value pairs.
2. It is generally used when we have a huge amount of data.
3. Dictionaries are optimized for retrieving data.
4. We must know the key to retrieve the value.
5. In Python, dictionaries are defined within braces **{}** with each item being a pair in the form __*key:value*__. Key and value can be of any type.

   ```Python3
   >>> d = {1:'value','key':2}
   >>> type(d)
   <class 'dict'>
   ```

6. We use key to retrieve the respective value. But not the other way around.

   ```Python3
   >>> d = {1:'value','key':2}
   >>> print(type(d))
   <class 'dict'>

   >>> print("d[1] = ", d[1])
   d[1] =  value

   >>> print("d['key'] = ", d['key'])
   d['key'] =  2

   >>> print("d[2] = ", d[2])
   Traceback (most recent call last):
     File "<string>", line 9, in <module>
   KeyError: 2
   ```

### Conversion between data types

1. We can convert between different data types by using different type conversion functions like __*int()*__, __*float()*__, __*str()*__, etc.

   ```Python3
   >>> float(5)
   5.0
   ```

2. Conversion from float to int will truncate the value (make it closer to zero).

   ```Python3
   >>> int(10.6)
   10

   >>> int(-10.6)
   -10
   ```

3. Conversion to and from string must contain compatible values.

   ```Python3
   >>> float('2.5')
   2.5

   >>> str(25)
   '25'

   >>> int('1p')
   Traceback (most recent call last):
     File "<string>", line 301, in runcode
     File "<interactive input>", line 1, in <module>
   ValueError: invalid literal for int() with base 10: '1p'
   ```

4. We can even convert one sequence to another.

   ```Python3
   >>> set([1,2,3])
   {1, 2, 3}

   >>> tuple({5,6,7})
   (5, 6, 7)

   >>> list('hello')
   ['h', 'e', 'l', 'l', 'o']
   ```

5. To convert to dictionary, each element must be a pair.

   ```python3
   >>> dict([[1,2],[3,4]])
   {1: 2, 3: 4}

   >>> dict([(3,26),(4,44)])
   {3: 26, 4: 44}
   ```

### References

1. [Real Python.](https://realpython.com/python-data-types/)
2. [Programiz.](https://www.programiz.com/python-programming/variables-datatypes)
